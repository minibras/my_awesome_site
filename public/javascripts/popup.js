    var popup = document.getElementById('popup');
    var btn = document.getElementById('contact');
    var del = document.getElementById('delete');

    btn.onclick = function () {
        popup.style.display = "block";
    }

    del.onclick = function () {
        popup.style.display = "none";
    }

    window.onclick = function (event) {
        if (event.target == popup) {
            popup.style.display = "none";
        }
    }
