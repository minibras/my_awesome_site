var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('dam', { title: 'Bienvenue sur mon site',
                    dam: true});
});

module.exports = router;